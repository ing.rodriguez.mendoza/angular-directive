import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [
  ]
})
export class AgregarComponent implements OnInit {

  color: string = 'blue';
  messsage: string = 'Debe de ingresar este campo';

  miForm: FormGroup = this.fb.group({
    nombre: [ '', [Validators.required, Validators.minLength(3)] ]
  });

  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
    console.log('AgregarComponent')
  }

  campoNoValido(campo: string){
      // return this.miForm.get(campo)?.errors && this.miForm.get(campo)?.touched;
      return this.miForm.get(campo)?.invalid || false;
  }

  cambiarNombre(){
    this.messsage = Math.random().toString();
  }

  cambiarColor(){
    const color = "#xxxxxx".replace(/x/g, y=>(Math.random()*16|0).toString(16));
    this.color = color;
  }
}
