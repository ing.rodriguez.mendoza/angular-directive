import { Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[error-msg]'
})

export class ErrorMsgDirective implements OnInit, OnChanges {

  htmlElement!: ElementRef<HTMLElement>;
  
  private _color:   string = 'red';
  private _message: string = 'Debe de ingresar este campo';
  private _noValido: boolean = true;

  /*
   * estos @Input responden al ngOnChanges
   */
  // @Input() color: string = 'red';
  // @Input() message!: string;
  @Input() set noValido(valor: boolean) {
    this._noValido = valor;

    if (valor) {
      this.htmlElement.nativeElement.hidden = !valor;
      return;
    }
    this.htmlElement.nativeElement.hidden = !valor;

    ////Other way by Profe fernando
    // if (valor) {
    //   console.log('valor set 1', valor);
    //   this.htmlElement.nativeElement.classList.remove('hidden');
      
    // }
    // else{
    //   console.log('valor set 2', valor);
    //   this.htmlElement.nativeElement.classList.add('hidden');
    // }
  };


  constructor(private element: ElementRef<HTMLElement>) { 
    console.log('error-msg  constructor');
    this.htmlElement = this.element;
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges the changes:', changes);
    // // this.setColor();
    // this.setClass();
    // // this.changeText();

    //#region 
     /*
    * Otra Forma no tan óptima para actualizar la informacion del formulario haciendo uso ngOnChanges
    *a futuro se puede volver muy complejo por el numero de campos que pueda tener el formulario
    */
    // if (changes['color']) {
    //   const color = changes['color'].currentValue;
    //   this.htmlElement.nativeElement.style.color = color;
    // }

    // if (changes['message']) {
    //   const message = changes['message'].currentValue;
    //   this.htmlElement.nativeElement.innerText = message;
    // }
    //#endregion

  }

  setColor() : void {
    this.htmlElement.nativeElement.style.color = this._color;
  }

  setClass() : void {
    this.htmlElement.nativeElement.className = 'form-text';
  }

  changeText() : void {
    this.htmlElement.nativeElement.textContent = this._message;
  }

  ngOnInit(): void {
    console.log('error-msg  ngOnInit');
    // console.log('valido', this.valido);
    this.setColor();
    this.setClass();
    this.changeText();
  }

  /*
  * Forma más óptima para actualizar la informacion del formulario 
  */
  @Input() set color(valor: string) {
    // this.htmlElement.nativeElement.style.color = valor;
    this._color = valor;
    this.setColor();
  }

  @Input() set message(valor: string) {
    // this.htmlElement.nativeElement.textContent = valor;
    this._message = valor;
    this.changeText();
  }



}
