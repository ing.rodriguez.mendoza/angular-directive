import { Directive, OnDestroy, OnInit, ElementRef, Input, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[customIf]'
})
export class CustomIfDirective implements OnInit, OnDestroy {

  htmElement!: ElementRef<HTMLElement>;


  @Input() set customIf(valor: boolean) {
    console.log('valido valor', valor);

    if (valor) {
      this.vc.createEmbeddedView( this.tr );
      
    } else {
      this.vc.clear();
      
    }

  }

  constructor(
    private tr:TemplateRef<HTMLElement>,
    private vc:ViewContainerRef) 
  {
    console.log('custom-if');
    //this.htmElement = this.vc;
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy custom-if');
  }
  ngOnInit(): void {
    console.log('ngOnInit custom-if');
    //this.htmElement.nativeElement.classList.add('hidden');
  }

}

  // constructor(private er:ElementRef<HTMLElement>) {
  //   console.log('custom-if');
  //   this.htmElement = this.er;
  // }

  // @Input() set valido(valor: boolean) {
  //   console.log('valido valor', valor);

  //   if (valor) {
  //     this.htmElement.nativeElement.classList.remove('hidden');
      
  //   } else {
  //     this.htmElement.nativeElement.classList.add('hidden');
      
  //   }

  // }